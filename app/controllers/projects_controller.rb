class ProjectsController < ApplicationController
  before_action :set_project, only: %i[ show edit update destroy ]

  # GET /projects
  def index
    @projects = Project.all
  end

  # GET /projects/1
  def show
  end

  # GET /projects/new
  def new
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /projects
  def create
    @project = Project.new(project_params)
    respond_to do |format|
    if @project.save
      format.turbo_stream
      format.html {redirect_to @project, notice: "Project was successfully created."}
    else
      format.html {render :new, status: :unprocessable_entity}
    end
    end
  end

  # PATCH/PUT /projects/1
  def update
    respond_to do |format|
    if @project.update(project_params)
      format.turbo_stream
      format.html { redirect_to projects_url, notice: "Project was successfully updated.", status: :see_other}
    else
      format.html {render :edit, status: :unprocessable_entity}
    end
    end
  end

  # DELETE /projects/1
  def destroy
    @project.destroy!
    redirect_to projects_url, notice: "Project was successfully destroyed.", status: :see_other
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def project_params
      params.require(:project).permit(:title)
    end
end
